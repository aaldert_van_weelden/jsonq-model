<?php

namespace Vwit\JsonQuery\Examples\App;


use Nahid\JsonQ\Jsonq;

class UserDto
{
    /**
     * 
     * @var string
     */
    public $id;
    
    /**
     *
     * @var string
     */
    public $role;
    
    /**
     *
     * @var string
     */
    public $firstname;
    
    /**
     *
     * @var string
     */
    public $lastname;
    
    /**
     *
     * @var string
     */
    public $fullname;
    
    /**
     *
     * @var string
     */
    public $username;
    
    /**
     * 
     * @param Jsonq $jsonq
     */
    public function __construct( Jsonq $jsonq = null)  
    {
        if( ! is_null($jsonq))
        {
            $this->id           = $jsonq->id;
            $this->role         = $jsonq->role;
            $this->firstname    = $jsonq->firstname;
            $this->lastname     = $jsonq->lastname;
            $this->fullname     = $jsonq->fullname;
            $this->username     = $jsonq->username;
        }
    }
    
    /**
     * 
     * @return Jsonq
     */
    public function jsonQ(): Jsonq
    {
        $jsonq = new Jsonq();
        $properties = $this->toArray();
        foreach ($properties as $name => $value)
        {
            $jsonq->{$name} = $value;
        }
        
        return $jsonq;
    }
    
    /**
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(get_object_vars($this));
    }
    
    /**
     *
     * @return string
     */
    public function toJson(): string
    {
        return json_encode( (object) $this->toArray());
    }
}