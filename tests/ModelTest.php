<?php

namespace Vwit\JsonQuery\Tests;

use Nahid\JsonQ\Jsonq;
use Vwit\JsonQuery\Exceptions\JsonqModelException;
use PHPUnit_Framework_TestCase;
use Vwit\JsonQuery\Dao\Model;
use Vwit\JsonQuery\DataSource\DS;
use Ramsey\Uuid\Uuid;

class ModelTest extends AbstractTestCase
{
    
    private $testObject;
    /**
     * 
     * {@inheritDoc}
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    protected function setUp()
    {
        $this->createFile();
        $this->createInvalidFile();
        $this->testObject = json_decode(self::$testData);
    } 

    /**
     * 
     * {@inheritDoc}
     * @see PHPUnit_Framework_TestCase::tearDown()
     */
    protected function tearDown()
    {
        $this->removeFiles();
    }
    
    public function testInit()
    {
        $model = new TestUser();
        
        $method = $this->makeCallable($model, 'init');

        $this->assertInstanceOf(TestUser::class, $method->invokeArgs($model, []));
    }
    
    /**
     * @expectedException  \Vwit\JsonQuery\Exceptions\JsonqModelException
     */
    public function testcreateWithoutID()
    {
        $dto = new Jsonq();
        TestUser::create($dto);
    }
    
   
    public function testcreateWithID()
    {
        $dto = new Jsonq();
        $dto->id = null;
        $model = TestUser::create($dto);
        $this->assertInstanceOf(TestUser::class, $model);
    }
    
    public function testModelCreatedUuid()
    {
        $dto = new Jsonq();
        $dto->id = null;
        $model = TestUser::create($dto);
        $this->assertNotNull($model->id);
        $this->assertTrue(Uuid::isValid($model->id));
    }
    
    public function testquery()
    {
        $this->assertInstanceOf(Jsonq::class, TestUser::query());
    }
   
    public function testall()
    {
        $this->assertNotNull(TestUser::all());
        $this->assertInstanceOf(Jsonq::class, TestUser::all());
        
        $this->assertEquals(count( (array) $this->testObject->users), TestUser::all()->count());
        $this->assertEquals( json_encode($this->testObject->users), TestUser::all()->toJson());
    }
    
    public function testsave()
    {
        $user = TestUser::create( TestUser::query()->first() );
        $username = $user->username;
        
        $newUsername = 'new_username';
        
        $this->assertNotEquals($newUsername, $username);
        
        $user->username = $newUsername;
        
        $updated = $user->save();
        
        $fetchedAfterUpdate = TestUser::create(TestUser::query()->first());
        
        $this->assertEquals($fetchedAfterUpdate, $updated);
        $this->assertEquals($fetchedAfterUpdate->username, $newUsername);
    }
    
    public function testdestroy()
    {
        $this->assertEquals( 2 , TestUser::all()->count());
        $user = TestUser::create( TestUser::query()->first() );
        
        $user->destroy();
        
        $this->assertEquals( 1 , TestUser::all()->count());
        
        $fetchedAfterUpdate = TestUser::create(TestUser::query()->first());
        
        $this->assertNotEquals($fetchedAfterUpdate->id, $user->id);
    }
  
    public function testdelete()
    {
        $this->assertEquals( 2 , TestUser::all()->count());
        
        $ids = [];
        TestUser::all()->each(function($id, $user) use(&$ids)
        { 
            $ids[] = $id;
        });
        
        $this->assertEquals(2, count($ids));
        
        TestUser::delete($ids);
        
        $this->assertEquals( 0 , TestUser::all()->count());
    }
     
    public function testfind()
    {
        $user = TestUser::query()->first();
        $id = $user->id;
        
        $found = TestUser::find($id);
        
        $this->assertEquals( $user->toJson() , $found->toJson());
    }
    
    public function testfindBy()
    {
        $user = TestUser::query()->first();
        $username = $user->username;
        
        $found = TestUser::findBy('username', $username);
        
        $this->assertEquals( $user->toJson() , $found->toJson());
    }
    
    /**
     * @expectedException  \Vwit\JsonQuery\Exceptions\JsonqModelException
     */
    public function testfindOrFail()
    {
        TestUser::findOrFail('non-existing');
    }
    
    public function testgetAllUsers()
    {
        $all = TestUser::all();
        $this->assertEquals( 2 , $all->count());
        
        $ids = [];
        $all->each(function($id, $user) use(&$ids)
        {
            $ids[] = $id;
        });
        
        $this->assertEquals(2, count($ids));
        
        $users = TestUser::findBySet($ids);

        $this->assertEquals(2 , $users->count());
        $this->assertEquals($all->toJson() , $users->toJson());
    }
    
    public function testgetNoUsers()
    {      
        $users = TestUser::findBySet([]);
        
        $this->assertEquals(0 , $users->count());
    }
    
    public function test__magiccallstatic()
    {
        $this->assertEquals(TestUser::query()->get(), TestUser::get());
    }
    
    /**
     * @expectedException Error
     */
    public function test__nonexistingmethod_magiccallstatic()
    {
        TestUser::query()->whatever();
    }
    
    public function test__magicget()
    {
        $user = TestUser::create( TestUser::query()->first() );
        
        $this->assertNotNull($user->id);
    }
    
    /**
     * @expectedException \PHPUnit_Framework_Error 
     */
    public function test__magicgetNonExisting()
    {
        $user = TestUser::create( TestUser::query()->first() );
        
        $this->assertFalse(property_exists($user, 'nonExistingProperty'));
        
        //triggers error
        $user->nonExistingProperty;

    }
    
    public function test__magicset()
    {
        $user = TestUser::create( TestUser::query()->first() );
        
        $this->assertFalse(property_exists($user, 'test'));
        
        $user->test = 2;
        
        $this->assertTrue(property_exists($user, 'test'));
        
        $this->assertEquals(2, $user->test);
    }
    
    public function testtoArray()
    {
        $model = TestUser::create( TestUser::query()->first() );
        
        $firstuser = $this->testObject->users->{$model->id};
        
        $model = TestUser::create( TestUser::query()->first() );
        
        $this->assertEquals( (array) $firstuser, $model->toArray());
    }
    
    public function testtoJson()
    {
        $model = TestUser::create( TestUser::query()->first() );
        
        $firstuser = $this->testObject->users->{$model->id};
        
        $model = TestUser::create( TestUser::query()->first() );
        
        $this->assertEquals( json_encode($firstuser), $model->toJson());
    }
  
}