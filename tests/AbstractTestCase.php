<?php

namespace Vwit\JsonQuery\Tests;

use Nahid\JsonQ\Jsonq;
use Vwit\JsonQuery\DataSource\DS;


abstract class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    
    const SERVER_PATH = 'tests/server/';
    const FILE_NAME = 'data.json';
    const FILE_INVALID_NAME = 'invalid_data.json';
    
    /**
     * @var Jsonq
     */
    protected $jsonq;
    
   /**
    * 
    * @var string
    */
    protected $serverpath;
    
    /**
     * @var string
     */
    protected $file;
    
    /**
     * @var string
     */
    protected $fileInvalid;
    
    protected static $testData = 
    '{
        "tablename": "users",
        "description": "Features users list with id as key",
        "users": {
            "5f719041-8574-4ba5-a14e-62442223a27c":
            {
            "role": "scoundrel",
            "id": "5f719041-8574-4ba5-a14e-62442223a27c",
            "firstname": "Smeagol",
            "lastname": "Hobbit",
            "fullname": "Smeagol Hobbit",
            "username": "s.hobbit@mordor.me"
            },
            "ae3f1ff1-818b-4a28-8a84-2517f0d57ef8":
            {
                "id": "ae3f1ff1-818b-4a28-8a84-2517f0d57ef8",
                "role": "hero",           
                "firstname": "Frodo",
                "lastname": "Baggins",
                "fullname": "Frodo Baggins",
                "username": "f.baggins@underhill.me"
            }
        }
    }';
 
    protected function createFile()
    {
        file_put_contents(self::SERVER_PATH.self::FILE_NAME, self::$testData);
        $this->file = self::SERVER_PATH.self::FILE_NAME;
    }
    
    protected function createInvalidFile()
    {
        file_put_contents(self::SERVER_PATH.self::FILE_INVALID_NAME, 'invalid_data');
        $this->fileInvalid = self::SERVER_PATH.self::FILE_INVALID_NAME;
    }
    
    protected function clearTestFile()
    {
        file_put_contents(self::SERVER_PATH.self::FILE_NAME, '');
    }
    
    protected function removeFiles()
    {
        unlink(self::SERVER_PATH.self::FILE_NAME);
        unlink(self::SERVER_PATH.self::FILE_INVALID_NAME);
        $this->file = null;
        $this->fileInvalid = null;
    }
    

    /**
     * Make private and protected function callable
     *
     * @param mixed $object
     * @param string $function
     * @return \ReflectionMethod
     */
    protected function makeCallable($object, $function)
    {
        $method = new \ReflectionMethod($object, $function);
        $method->setAccessible(true);

        return $method;
    }
}