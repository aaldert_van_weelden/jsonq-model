<?php

namespace Vwit\JsonQuery\Tests;

use Vwit\JsonQuery\Dao\Model;



/**
 * 
 * @author Aaldert
 *
 */
class TestUser extends Model
{ 
    /**
     * 
     * @var string
     */
    protected $datasource = 'data';
    
   /**
    * 
    * @var string
    */
    protected $table = 'users';
    
    
}