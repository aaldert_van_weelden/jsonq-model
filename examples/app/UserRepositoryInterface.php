<?php

namespace Vwit\JsonQuery\Examples\App;


use Nahid\JsonQ\Jsonq;

interface UserRepositoryInterface
{
    /**
     * 
     * @param string $username
     * @return User|NULL
     */
    public function findByUsername(string $username): ?User;
    
    /**
     * 
     * @param string[] $ids
     * @return []User
     */
    public function getUsersByIds( array $ids): Jsonq;
    
    /**
     * 
     * @return Jsonq
     */
    public function findAll(): Jsonq;
    
    /**
     * 
     * @param Jsonq $dto
     * @return User
     */
    public function make( Jsonq $dto ): User;
    
    /**
     * 
     * @param array $ids
     * @return bool
     */
    public function deleteByIds(array $ids): bool;
}