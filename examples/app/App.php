<?php 

namespace Vwit\JsonQuery\Examples\App;

use Vwit\JsonQuery\DataSource\DS;
use Nahid\JsonQ\Jsonq;

require_once '../../vendor/autoload.php';

/**
 * Appication singleton
 * @author Aaldert
 *
 */
class App
{
    /**
     * 
     * @var self
     */
    private static $instance;
    
    /**
     * 
     * @var UserRepositoryInterface
     */
    private $userRepository;
    
    
    public static function init(): self
    {
        $_ENV[DS::JSONQ_SERVER_PATH] = realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'server';

        if(! self::$instance) self::$instance = new static();
        
        self::$instance->userRepository = UserRepository::instance();
        
        return self::$instance;
    }
    
    /**
     * 
     * @return User
     */
    public function createUser(): User
    {
        $dto = new UserDto();
        $dto->role =  "admin";
        $dto->firstname = "Aaldert";
        $dto->lastname = "van Weelden";
        $dto->fullname = "Aaldert van Weelden";
        $dto->username = "a.vanweelden@vwit.nl";
        
        $user = $this->userRepository->make($dto->jsonQ());
        
        $persisted = $user->save();
        
        print_r($persisted);
        
        
    }
    
    /**
     * 
     * @return User
     */
    public function readUser(): User
    {
        $user = $this->userRepository->findByUsername('s.hobbit@mordor.me');
        
        print_r($user);
        
        return $user;
    }
    
    /**
     * 
     * @return User
     */
    public function updateUser(): User
    {
            $user = $this->userRepository->findByUsername('s.hobbit@mordor.me');
    
            print_r($user);
    
            $user->role = 'master';
    
            $persisted = $user->save();
    
            $found = $this->userRepository->findByUsername('s.hobbit@mordor.me');
    
            print_r($found);
            print_r($persisted);
    
            $found->role = "good guy";
            $found->save();
    
            $refreshed =  $this->userRepository->findByUsername('s.hobbit@mordor.me');
            print_r($refreshed);
            
            return $refreshed;
    }
    
    /**
     * 
     * @return User
     */
    public function destroyUser(): User
    {
        $staleObject = $persisted->destroy();

        $found = $this->userRepository->findByUsername('a.vanweelden@vwit.nl');
        
        var_dump($found);
        
        return $staleObject;
    }
    
   /**
    * 
    * @return bool
    */
    public function deleteUsers(): bool
    {
        $users = $this->userRepository->findAll();
        
        $ids = [];
        $users->each(function($user) use($ids)
        {
            $ids[] = $user->id;
        });
        
        return $this->userRepository->deleteByIds($ids);
    }
    
    /**
     * 
     */
    public function run():void
    {
        print "App is running...\n";
        
        $this->createUser();
        
        $this->readUser();
        
        $this->updateUser();
        
        $this->destroyUser();
        
        $this->deleteUsers();
    }
}


App::init()->run();
