<?php

namespace Vwit\JsonQuery\Dao;


use Nahid\JsonQ\Jsonq;
use Nahid\QArray\Clause;
use Vwit\JsonQuery\DataSource\DS;
use Ramsey\Uuid\Uuid;
use Vwit\JsonQuery\Exceptions\JsonqModelException;

/**
 * 
 * @author Aaldert
 * Let your models extend from this class
 *
 */
abstract class Model
{
    
    /**
     * The datasource name
     * @var string
     */
    protected $datasource;
    
    /**
     * The referenced table name
     * @var string|NULL
     */
    protected $table; 
    
    /**
     * 
     * @var self
     */
    private static $instance;

    /**
     *
     * @return self
     */
    private static function init(): self
    {
        self::$instance = new static();
        
        //default table is the lowercase classname pluralis
        if(empty(self::$instance->table)) self::$instance->table = strtolower( (new \ReflectionClass(self::$instance))->getShortName() ).'s';
        
        if(empty(self::$instance->datasource)) self::$instance->datasource = $_ENV['JSONQ_SERVER_DATASOURCE'] ?? null;
        
        if(empty(self::$instance->datasource)) self::$instance->datasource = self::$instance->table;
        
        return self::$instance;
    }
    
    /**
     * Get the datasource table instance
     * @return Jsonq
     * @throws JsonqModelException
     */
    public static function query(): Jsonq
    { 
        try 
        {
            self::init();
            
            return  DS::connect(self::$instance->datasource)->query()->from(self::$instance->table);
        } 
        catch (\Exception $e) 
        {
            throw new JsonqModelException(__FUNCTION__."(): Connection error". $e);
        }
        
    }
    
    /**
     *
     * @param Jsonq $dto
     * @return self
     * @throws JsonqModelException
     */
    public static function create(Jsonq $dto): self
    {
        try 
        {
            if( ! array_key_exists('id', $dto->toArray()) ) throw new \Exception("DTO id property is missing");
            
            if( is_null($dto->id) ) $dto->id = Uuid::uuid4()->toString();
            
            $model = self::init();
            
            $dto->each(function($key, $value) use($model)
            {
                $model->{$key} = $value;
            });
            
            return $model;
        } 
        catch (\Exception $e) 
        {
            throw new JsonqModelException(__FUNCTION__."(): failed to create a new entity", $e);
        }
        
    }
     
    /**
     *
     * @return Jsonq
     */
    public static function all(): Jsonq
    {
        return self::query()->get();
    }
    
    /**
     * Persist the current entity
     * @return self|NULL
     * @throws JsonqModelException
     */
    public function save(): ?self
    {
        try
        {
            $datasource = DS::connect(self::$instance->datasource)->read();
            
            $datasource->{self::$instance->table}->{$this->id} = $this;
            
            DS::connect(self::$instance->datasource)->write($datasource);
            
            $refreshed = (object) self::find($this->id);
            
            return self::create($refreshed);
        }
        catch (\Exception $e)
        {
            throw new JsonqModelException( __FUNCTION__."(): Failed to persist entity: ".$this->id, $e);
        }

    }
    
    /**
     * Remove the current entity
     * @return self The destroyed entity
     * @throws JsonqModelException
     */
    public function destroy(): self
    {
        try
        {
            $datasource = DS::connect(self::$instance->datasource)->read();
            
            unset($datasource->{self::$instance->table}->{$this->id});
            
            DS::connect(self::$instance->datasource)->write($datasource);
            
            return $this;
        }
        catch (\Exception $e)
        {
            throw new JsonqModelException( __FUNCTION__."(): Failed to remove entity: ".$this->id, $e);
        }
    }
    
    /**
     *
     * @param array $ids
     * @return bool
     * @throws JsonqModelException
     */
    public static function delete(array $ids): bool
    {
        try
        {
            $datasource = DS::connect(self::$instance->datasource)->read();
            
            foreach ($ids as $id)
            {
                unset($datasource->{self::$instance->table}->{$id});
            }

            DS::connect(self::$instance->datasource)->write($datasource);
            
            return true;
        }
        catch (\Exception $e)
        {
            throw new JsonqModelException( __FUNCTION__."(): Failed to delete entity: ".$id, $e);
        }
    }
    
    /**
     *
     * @param string $id
     * @return Jsonq|NULL
     */
    public static function find(string $id): ?Jsonq
    {
        return self::findBy('id', $id);
    }
    
    /**
     * 
     * @param string $key
     * @param mixed $value
     * @return Jsonq|NULL
     */
    public static function findBy(string $key, $value): ?Jsonq
    {
        return self::query()->where($key, '=' ,$value)->first();
    }
    
    /**
     *
     * @param string $id
     * @return Jsonq
     */
    public static function findOrFail(string $id): Jsonq
    {
        $res = self::find($id);
        
        if( is_null($res) ) throw new JsonqModelException(__FUNCTION__."(): Failed to find entity ".self::$instance->table." $id");
        
        return $res;
    }
    
    /**
     * Find the entities by key, defaultkey is id
     * @param array $keys
     * @return Jsonq
     */
    public static function findBySet(array $values, string $key = 'id'): Jsonq
    {
        $query = self::query()->where(1, '=', 1);
        foreach ($values as $value)
        {
            $query->orWhere($key, '=', $value);
        }
        return $query->get();
    }
    
    /**
     * Direct access to Jsonq class methods for convenience
     * @example Model::first()
     * @example Model::where()->get()->toJson()
     * @param string $func
     * @param array $args
     * @return Jsonq
     */
    public static function __callstatic(string $func, array $args): Jsonq
    {
        return self::query()->{$func}(...$args);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->{$name};
    }
    
    /**
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $this->{$name} = $value;
    }
    
    /**
     *
     * @return array
     */
    public function toArray(): array
    {
        $props = get_object_vars($this);
        unset($props['datasource']);
        unset($props['table']);
        
        return $props;
    }
    
    /**
     *
     * @return string
     */
    public function toJson(): string
    {
        return json_encode( (object) $this->toArray());
    }
 
}