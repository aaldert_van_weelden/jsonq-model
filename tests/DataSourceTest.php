<?php


namespace Vwit\JsonQuery\Tests;

use PHPUnit_Framework_TestCase;
use Vwit\JsonQuery\DataSource\DS;
use Nahid\QArray\Exceptions\InvalidJsonException;
use Nahid\JsonQ\Jsonq;

class DataSourceTest extends AbstractTestCase
{
    /**
     * 
     * @var object
     */
    private $testObject;
    
    /**
     * 
     * {@inheritDoc}
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    protected function setUp()
    {
        $this->createFile();
        $this->createInvalidFile();
        
        $this->testObject = json_decode(self::$testData);
    } 

    /**
     * 
     * {@inheritDoc}
     * @see PHPUnit_Framework_TestCase::tearDown()
     */
    protected function tearDown()
    {
        $this->removeFiles();
    } 
    
    public function testDummy()
    {
        $this->assertTrue(1==1);
    }
    
    /**
     * @expectedException  \Nahid\QArray\Exceptions\InvalidJsonException
     */
    public function testNonExistingConnection()
    {
        DS::connect('non-existing');
    }
    
    /**
     * @expectedException  \Nahid\QArray\Exceptions\InvalidJsonException
     */
    public function testInvalidConnection()
    {  
        $datasource = rtrim(self::FILE_INVALID_NAME, '.json');
        DS::connect($datasource);
    }
    
    /**
     * @expectedException  \Nahid\QArray\Exceptions\InvalidJsonException
     */
    public function testEmptyFileConnection()
    {
        $this->clearTestFile();
        $datasource = rtrim(self::FILE_NAME, '.json');
        DS::connect($datasource);
    }
    
    public function testValidConnection()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        $connection = DS::connect($datasource);
        
        $this->assertInstanceOf(DS::class, $connection);
    }
    
    
    public function testQuery()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        $jsonq = DS::connect($datasource)->query();
        
        $this->assertInstanceOf(Jsonq::class, $jsonq);
        
        $this->assertEquals( json_encode($this->testObject), $jsonq->get()->toJson());
    }
    
    
    public function testRead()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        $data = DS::connect($datasource)->read();
        
        $this->assertEquals( $this->testObject, $data);
    }
    
    public function testWriteEmpty()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        
        DS::connect($datasource)->write([]);
        
        $data = DS::connect($datasource)->read();
        $jsonq = DS::connect($datasource)->query()->get();
        
        $this->assertEmpty($data);
        $this->assertEquals( 0, $jsonq->count() );
    }
    
    public function testWrite()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        
        DS::connect($datasource)->write([]);
        DS::connect($datasource)->write($this->testObject);
        
        $data = DS::connect($datasource)->read();
        $jsonq = DS::connect($datasource)->query();
        
        $this->assertEquals( $this->testObject, $data);
        
        $this->assertEquals( json_encode($this->testObject), $jsonq->get()->toJson());        
    }
     
    /**
     * @group check
     */
    public function testReadWriteCorrelation()
    {
        $datasource = rtrim(self::FILE_NAME, '.json');
        
        DS::connect($datasource)->write($this->testObject);
        
        $fetched = DS::connect($datasource)->read();
        
        $this->assertEquals( $this->testObject, $fetched);
        
        DS::connect($datasource)->write($fetched);
        
        $fetchedAgain = DS::connect($datasource)->read();
        
        $this->assertEquals( $fetched, $fetchedAgain);

    }
}