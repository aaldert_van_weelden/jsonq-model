<?php

namespace Vwit\JsonQuery\Examples\App;

use Vwit\JsonQuery\Dao\Model;



/**
 * 
 * @author Aaldert
 * @uses \Database\Model
 *
 */
class User extends Model
{ 
    /**
     * 
     * @var string
     */
    protected $datasource = 'userdatasource';
    
   /**
    * 
    * @var string
    */
    protected $table = 'users';
    
    
}