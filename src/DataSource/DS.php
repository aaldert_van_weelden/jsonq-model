<?php

namespace Vwit\JsonQuery\DataSource;

use Nahid\JsonQ\Jsonq;

class DS
{
    /**
     * Environment key defining the application base path, usually defined in a .env file
     * @var string
     */
    const JSONQ_SERVER_PATH = 'JSONQ_SERVER_PATH';
    /**
     * 
     * @var Jsonq
     */
    private $ds;

    /**
     * 
     * @var string
     */
    private $path;
    
    /**
     * 
     * @var self
     */
    private static $instance;
    
    
    private function __construct($datasource) 
    {
        $this->path = $_ENV[DS::JSONQ_SERVER_PATH].DIRECTORY_SEPARATOR.$datasource.'.json';
        
        $this->ds = new Jsonq($this->path);
    }
    
    /**
     * 
     * @param string $name
     * @return self
     */
    public static function connect($datasource)
    {
        return new self($datasource);
    }
    
    /**
     * 
     * @return \Nahid\JsonQ\Jsonq
     */
    public function query()
    {
        return $this->ds;
    }
    
    /**
     * Read the whole data resource from json file
     * @return object
     */
    public function read()
    {
        return json_decode( file_get_contents($this->path) );
    }
    
    /**
     * Write the whole data resource to json file
     * @param string|object $all
     */
    public function write($all)
    {
        if ( ! is_string($all)) $all = json_encode($all, JSON_PRETTY_PRINT);
        file_put_contents($this->path, $all);
    }
    
    
    
}


