<?php

namespace Vwit\JsonQuery\Examples\App;

use Nahid\JsonQ\Jsonq;


class UserRepository implements UserRepositoryInterface
{
    
    /**
     * 
     * @var self
     */
    private static $instance;
    
    /**
     * 
     * @return self
     */
    public static function instance():self
    {
        if( ! self::$instance ) self::$instance = new static();
        
        return self::$instance;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Vwit\JsonQuery\Examples\App\UserRepositoryInterface::findByUsername()
     */
    public function findByUsername(string $username): ?User
    {
        $res = User::findBy('username', $username);
        
        if( is_null($res) ) return null;
        
        return User::create($res);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \Vwit\JsonQuery\Examples\App\UserRepositoryInterface::getUsersByIds()
     */
    public function getUsersByIds(array $ids): Jsonq
    {
        
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vwit\JsonQuery\Examples\App\UserRepositoryInterface::findAll()
     */
    public function findAll(): Jsonq
    {
        return User::all();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vwit\JsonQuery\Examples\App\UserRepositoryInterface::make()
     */
    public function make(Jsonq $dto): User
    {
        return User::create($dto);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vwit\JsonQuery\Examples\App\UserRepositoryInterface::deleteByIds()
     */
    public function deleteByIds(array $ids): bool
    {
        return User::delete($ids);
    }


   
}