<?php 
namespace Vwit\JsonQuery\Exceptions;
/**
 * ConsumerClient exception class
 * 
 * @author Aaldert van Weelden
 */
class JsonqModelException extends \Exception{
	
	const TAG = "JSONQ MODEL EXCEPTION ";
	const CODE = 100;
	
	/**
	 * 
	 * @var string
	 */
	protected $message;
	
	/**
	 * 
	 * @var integer
	 */
	protected $code;
	
	public function __construct(string $message = null, \Exception $e = null, int  $code=0)
	{
		$this->message=$message;
		if( $e instanceof \Exception ) $this->message.= ', '.$this->getShortName($e->getFile()).' line '.$e->getLine().': '.$e->getMessage();
		$this->code=$code;
		if($code==0) $this->code= self::CODE;
		
		parent::__construct(self::TAG.$this->message, $this->code);
	}
	
	/**
	 * 
	 * @param string $classPath
	 * @return string classname
	 */
	private function getShortName(string $classPath): string
	{
	    $parts = explode("\\",$classPath);    
	    return  array_pop($parts);
	}
	
	protected function setMessage($message)
	{
		$this->message=$message;
	}
	
	protected function setCode($code)
	{
		$this->code=$code;
	}
	
    // custom string representation of object
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}